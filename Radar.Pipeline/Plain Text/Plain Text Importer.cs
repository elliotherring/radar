﻿using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Radar.Import;

namespace Radar.Pipeline {

	//// USAGE
	//// Text text = Content.Load<Text>("TextContent");
	//// str = System.Text.Encoding.UTF8.GetString(text.CompiledText, 0, text.CompiledText.Length);
		
	// Text importer
	[ContentImporter(".txt", DisplayName = "Plain Text Importer - Radar", DefaultProcessor = "Plain Text Processor - Radar")]
	public class TextImporter: ContentImporter <TextInput> {
		// Override the Import() method
		public override TextInput Import(string filename, ContentImporterContext context) 
		{
			// Read the text data from .txt file
			string text = System.IO.File.ReadAllText(filename);
			// Return a new TextInput object
			return new TextInput(text);
		}
	}
	
	// Text content processor
	[ContentProcessor( DisplayName = "Plain Text Processor - Radar" )]
	public class TextProcessor : ContentProcessor <TextInput, TextInput> 
	{
		// Override the Process() method
		public override TextInput Process(TextInput input, ContentProcessorContext context) 
		{
			// Return the Text object
			//return new TextInput(Encoding.UTF8.GetBytes(input.Text));
			return input;
		}
	}
	
	// TextWriter for writing the text information into XNB file
	[ContentTypeWriter]
	public class TextWriter: ContentTypeWriter < TextInput > 
	{
		// Override the Write() method
		protected override void Write(ContentWriter output, TextInput value) 
		{
			// Write the text length information to XNB file
			var text = Encoding.UTF8.GetBytes(value.Text);
			output.Write(text.Length);
			// Write the text string into XNB file
			output.Write(text);
		}
		public override string GetRuntimeType(TargetPlatform targetPlatform) 
		{
			// Get the run time type of Text
			return "Radar.Import.Text, Radar.Engine";
		}
		public override string GetRuntimeReader(TargetPlatform targetPlatform) 
		{
			// Get the Text assembly information
			return "Radar.Import.TextReader, Radar.Engine";
		}
	}
}