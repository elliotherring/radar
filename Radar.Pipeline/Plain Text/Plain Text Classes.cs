﻿using System.Text;

namespace Radar.Pipeline {

	// TextInput class for importer and processor
	public class TextInput {
		// Text string
		string text;

		// Constructor
		public TextInput(string text) {
			this.text = text;
		}
		
		// Text property
		public string Text {
			get {
				return text;
			}
		}
	}
}