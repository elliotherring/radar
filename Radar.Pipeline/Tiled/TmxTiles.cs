﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;

namespace Radar.Pipeline
{
	public class TmxTileLayer : TmxLayer
	{
		[XmlAttribute( AttributeName = "x" )]
		public int x;

		[XmlAttribute( AttributeName = "y" )]
		public int y;

		[XmlAttribute( AttributeName = "width" )]
		public int width;

		[XmlAttribute( AttributeName = "height" )]
		public int height;

		[XmlElement( ElementName = "data" )]
		public TmxData data;
	}
	
	[XmlRoot( ElementName = "tileset" )]
	public class TmxTileset
	{
		// we need this for tilesets that have no image. they use image collections and we need the path to save the new atlas we generate.
		public string mapFolder;
		public bool isStandardTileset = true;

		[XmlAttribute( AttributeName = "firstgid" )]
		public int firstGid;

		[XmlAttribute( AttributeName = "source" )]
		public string source;

		[XmlAttribute( AttributeName = "name" )]
		public string name;

		[XmlAttribute( AttributeName = "tilewidth" )]
		public int tileWidth;

		[XmlAttribute( AttributeName = "tileheight" )]
		public int tileHeight;

		[XmlAttribute( AttributeName = "spacing" )]
		public int spacing;

		[XmlAttribute( AttributeName = "margin" )]
		public int margin;

		[XmlAttribute( AttributeName = "tilecount" )]
		public int tileCount;

		[XmlAttribute( AttributeName = "columns" )]
		public int columns;

		[XmlElement( ElementName = "tileoffset" )]
		public TmxTileOffset tileOffset;

		[XmlElement( ElementName = "tile" )]
		public List<TmxTilesetTile> tiles;

		[XmlArray( "properties" )]
		[XmlArrayItem( "property" )]
		public List<TmxProperty> properties;

		[XmlElement( ElementName = "image" )]
		public TmxImage image;

		[XmlArray( "terraintypes" )]
		[XmlArrayItem( "terrain" )]
		public List<TmxTerrain> terrainTypes;

		public TmxTileset()
		{
			tileOffset = new TmxTileOffset();
			tiles = new List<TmxTilesetTile>();
			properties = new List<TmxProperty>();
			terrainTypes = new List<TmxTerrain>();
		}

		public void fixImagePath( string mapPath, string tilesetSource )
		{
			var mapDirectory = Path.GetDirectoryName( mapPath );
			var tilesetDirectory = Path.GetDirectoryName( tilesetSource );
			var imageDirectory = Path.GetDirectoryName( this.image.source );
			var imageFile = Path.GetFileName( this.image.source );
            
			var newPath = Path.GetFullPath( Path.Combine( mapDirectory, tilesetDirectory, imageDirectory, imageFile ) );                        
			image.source = Path.Combine( PathHelper.makeRelativePath( mapPath, newPath ) );
		}


		public override string ToString()
		{
			return string.Format( "{0}: {1}", name, image );
		}

	}
	
	public class TmxTerrain
	{
		[XmlAttribute( AttributeName = "name" )]
		public string name;

		[XmlAttribute( AttributeName = "tile" )]
		public int tileId;

		[XmlArray( "properties" )]
		[XmlArrayItem( "property" )]
		public List<TmxProperty> properties = new List<TmxProperty>();


		public override string ToString()
		{
			return name;
		}
	}
	
	[XmlRoot( ElementName = "tileoffset" )]
	public class TmxTileOffset
	{
		public TmxTileOffset()
		{}
			

		[XmlAttribute( AttributeName = "x" )]
		public int X;

		[XmlAttribute( AttributeName = "y" )]
		public int Y;


		public override string ToString()
		{
			return string.Format( "{0}, {1}", X, Y );
		}
	}
	
	public class TmxTilesetTile
	{
		[XmlAttribute( AttributeName = "id" )]
		public int id;

		[XmlElement( ElementName = "terrain" )]
		public TmxTerrain terrain;

		[XmlAttribute( AttributeName = "probability" )]
		public float probability = 1f;

		[XmlElement( ElementName = "image" )]
		public TmxImage image;

		[XmlElement( ElementName = "objectgroup" )]
		public List<TmxObjectGroup> objectGroups;

		[XmlArray( "properties" )]
		[XmlArrayItem( "property" )]
		public List<TmxProperty> properties = new List<TmxProperty>();

		[XmlArray( "animation" )]
		[XmlArrayItem( "frame" )]
		public List<TmxTilesetTileAnimationFrame> animationFrames;

		/// <summary>
		/// source Rectangle for tilesets that use the collection of images
		/// </summary>
		public Rectangle sourceRect;


		public override string ToString()
		{
			return string.Format( "[TmxTilesetTile] id: {0}, animationFrames: {1}, image: {2}", id, animationFrames.Count, image );
		}
	}

	[XmlRoot( ElementName = "frame" )]
	public class TmxTilesetTileAnimationFrame
	{
		[XmlAttribute( AttributeName = "tileid" )]
		public int tileId;

		[XmlAttribute( AttributeName = "duration" )]
		public float duration;


		public override string ToString()
		{
			return string.Format( "[TmxTilesetTileAnimationFrame] tileId: {0}, duration: {1}", tileId, duration );
		}
	}
	
	public class TmxProperty
    {
		public TmxProperty()
		{}

		[XmlAttribute(AttributeName = "name")]
		public string name;

		[XmlAttribute(AttributeName = "value")]
		public string value;

        public override string ToString()
        {
            return string.Format("{0}: {1}", name, value);
        }
    }
    
   	public enum TiledLayerType
	{
		Tile,
		Image
	}
}

