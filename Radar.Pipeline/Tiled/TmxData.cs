﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Radar.Pipeline
{

	public class TmxData
	{
		public TmxData()
		{}

		[XmlAttribute( AttributeName = "encoding" )]
		public string encoding;

		[XmlAttribute( AttributeName = "compression" )]
		public string compression;

		[XmlElement( ElementName = "tile" )]
		public List<TmxDataTile> tiles = new List<TmxDataTile>();

		[XmlText]
		public string value;


		public override string ToString()
		{
			return string.Format( "{0} {1}", encoding, compression );
		}
	}

	public class TmxDataTile
	{
		public TmxDataTile()
		{
		
		}
		
		public TmxDataTile( uint gid )
		{
			this.gid = gid;
		}

		[XmlAttribute( AttributeName = "gid" )]
		public uint gid;
		public bool flippedHorizontally;
		public bool flippedVertically;
		public bool flippedDiagonally;

		public override string ToString()
		{
			return gid.ToString();
		}
	}
	}
