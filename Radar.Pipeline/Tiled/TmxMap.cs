﻿using Microsoft.Xna.Framework;
using System;
using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Radar.Pipeline
{
	[XmlRoot( ElementName = "map" )]
	public class TmxMap
	{
		public TmxMap()
		{
			properties = new List<TmxProperty>();
			tilesets = new List<TmxTileset>();
			layers = new List<TmxLayer>();
			objectGroups = new List<TmxObjectGroup>();
		}

		[XmlAttribute( AttributeName = "firstgid" )]
		public int firstGid;

		[XmlAttribute( AttributeName = "width" )]
		public ushort width;

		[XmlAttribute( AttributeName = "height" )]
		public ushort height;

		[XmlAttribute( AttributeName = "tilewidth" )]
		public byte tileWidth;

		[XmlAttribute( AttributeName = "tileheight" )]
		public byte tileHeight;

		[XmlElement( ElementName = "tileset" )]
		public List<TmxTileset> tilesets;

		[XmlElement( ElementName = "objectgroup" )]
		public List<TmxObjectGroup> objectGroups;

		[XmlElement( ElementName = "layer", Type = typeof( TmxTileLayer ) )]
		[XmlElement( ElementName = "imagelayer", Type = typeof( TmxImageLayer ) )]
		public List<TmxLayer> layers;

		[XmlArray( "properties" )]
		[XmlArrayItem( "property" )]
		public List<TmxProperty> properties;
	}
    
    [XmlInclude( typeof( TmxTileLayer ) )]
	[XmlInclude( typeof( TmxImageLayer ) )]
	public abstract class TmxLayer
	{
		[XmlAttribute( AttributeName = "offsetx" )]
		public float offsetx;

		[XmlAttribute( AttributeName = "offsety" )]
		public float offsety;

		[XmlAttribute( AttributeName = "name" )]
		public string name;

		[XmlAttribute( AttributeName = "opacity" )]
		public float opacity = 1f;

		[XmlAttribute( AttributeName = "visible" )]
		public bool visible = true;

		[XmlArray( "properties" )]
		[XmlArrayItem( "property" )]
		public List<TmxProperty> properties = new List<TmxProperty>();


		public override string ToString()
		{
			return string.Format( "[TmxLayer] name: {0}, visible: {1}", name, visible );
		}
	}
	
}