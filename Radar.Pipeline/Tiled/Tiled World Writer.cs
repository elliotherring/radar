﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Xml.Serialization;
//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Content;
//using Microsoft.Xna.Framework.Content.Pipeline;
//using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

//namespace Radar.Pipeline
//{
//	[ContentTypeWriter]
//	public class TiledWorldWriter : ContentTypeWriter<TmxMap>
//	{
//		const uint FLIPPED_VERTICALLY_FLAG = 0x40000000;
//		const uint FLIPPED_HORIZONTALLY_FLAG = 0x80000000;
//		const uint FLIPPED_DIAGONALLY_FLAG = 0x20000000;
//															//TODO wrong format
//		protected override void Write(ContentWriter writer, TmxMap map)
//		{
//			// Map
//			writer.Write(map.width);
//			writer.Write(map.height);

//			// Tileset
//			var tileset = map.tilesets[0];
//			writer.Write(removeExtension(tileset.image.source));
//			writer.Write(tileset.firstGid);
//			writer.Write(tileset.tileWidth);
//			writer.Write(tileset.tileHeight);
//			writer.Write(tileset.spacing);
//			writer.Write(tileset.margin);

//			// Animated Tiles
//			var animtiles = tileset.tiles.Where(x => x.animationFrames.Count > 0);
//			writer.Write(animtiles.Count());

//			// TODO handle tiles with properties
//			// including flipped tiles
//			foreach (var tile in animtiles)
//			{
//				if (tile.animationFrames.Count == 0) continue;
//				TiledMapProcessor.logger.LogMessage("writing animated tile: {0}", tile);
//				writer.Write((ushort)(tile.id + 1));

//				//// animation frames
//				writer.Write(tile.animationFrames.Count);
//				foreach (var anim in tile.animationFrames)
//				{
//					writer.Write((ushort)(anim.tileId + 1));
//					writer.Write((float)anim.duration);
//				}
//			}

//			//TODO rooms
//			foreach (var room in rooms)
//			{
//				writer.Write(room.X);
//				writer.Write(room.Y);
//				writer.Write(removeExtension(room.source));
//			}
//		}

//		static string removeExtension(string path)
//		{
//			var dotIndex = path.LastIndexOf('.');
//			return dotIndex > 0 ? path.Substring(0, dotIndex) : path;
//		}

//		public override string GetRuntimeType(TargetPlatform targetPlatform)
//		{
//			return typeof(Radar.Tiled.World).AssemblyQualifiedName;
//		}


//		public override string GetRuntimeReader(TargetPlatform targetPlatform)
//		{
//			return typeof(Radar.Tiled.TiledWorldReader).AssemblyQualifiedName;
//		}

//	}
//}