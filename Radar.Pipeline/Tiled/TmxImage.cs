﻿using System;
using System.Xml.Serialization;

namespace Radar.Pipeline
{
	public class TmxImageLayer : TmxLayer
	{
		[XmlElement( ElementName = "image" )]
		public TmxImage image;

		public override string ToString()
		{
			return string.Format( "[TmxImageLayer] name: {0}, image: {1}, visible: {2}", name, image, visible );
		}
	}

	public class TmxImage
	{
		[XmlAttribute( AttributeName = "source" )]
		public string source;

		[XmlAttribute( AttributeName = "width" )]
		public int width;

		[XmlAttribute( AttributeName = "height" )]
		public int height;

		[XmlAttribute( AttributeName = "format" )]
		public string format;

		[XmlAttribute( AttributeName = "trans" )]
		public string trans;

		[XmlElement( ElementName = "data" )]
		public TmxData data;

		public override string ToString()
		{
			return source;
		}
	}

}
