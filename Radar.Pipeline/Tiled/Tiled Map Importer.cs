﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

namespace Radar.Pipeline
{
	[ContentImporter(".tmx", DefaultProcessor = "Tiled Map Processor - Nez", DisplayName = "Tiled Map Importer - Nez")]
	public class TiledMapImporter : ContentImporter<TmxMap>
	{
		public override TmxMap Import(string filename, ContentImporterContext context)
		{
			if (filename == null)
				throw new ArgumentNullException(nameof(filename));

			using (var reader = new StreamReader(filename))
			{
				context.Logger.LogMessage("Deserializing filename: {0}", filename);

				var serializer = new XmlSerializer(typeof(TmxMap));
				var map = (TmxMap)serializer.Deserialize(reader);
				var xmlSerializer = new XmlSerializer(typeof(TmxTileset));

				foreach (var l in map.layers)
					context.Logger.LogMessage("Deserialized Layer: {0}", l);

				foreach (var o in map.objectGroups)
					context.Logger.LogMessage("Deserialized ObjectGroup: {0}, object count: {1}", o.name, o.objects.Count);

				context.Logger.LogMessage("Tileset count: {0}", map.tilesets.Count);

				for (var i = 0; i < map.tilesets.Count; i++)
				{
					context.Logger.LogMessage("Reading tileset {0}", map.tilesets.Count);
					var tileset = map.tilesets[i];
					if (!string.IsNullOrWhiteSpace(tileset.source))
					{
						var directoryName = Path.GetDirectoryName(filename);
						var tilesetLocation = tileset.source.Replace('/', Path.DirectorySeparatorChar);
						var filePath = Path.Combine(directoryName, tilesetLocation);

						var normExtTilesetPath = new DirectoryInfo(filePath).FullName;
						context.Logger.LogMessage("Reading External Tileset File: " + normExtTilesetPath);
						using (var file = new StreamReader(filePath))
						{
							map.tilesets[i] = (TmxTileset)xmlSerializer.Deserialize(file);
							map.tilesets[i].fixImagePath(filename, tileset.source);
							map.tilesets[i].firstGid = tileset.firstGid;
						}
					}
					else
					{
						tileset.mapFolder = Path.GetDirectoryName(Path.GetFullPath(filename));
					}
				}
				context.Logger.LogMessage("Finished reading map.");
				return map;
			}
		}
	}

	[ContentProcessor(DisplayName = "Tiled Map Processor - Nez")]
	public class TiledMapProcessor : ContentProcessor<TmxMap, TmxMap>
	{
		public static ContentBuildLogger logger;

		public override TmxMap Process(TmxMap map, ContentProcessorContext context)
		{
			logger = context.Logger;
			foreach (var layer in map.layers.OfType<TmxTileLayer>())
			{
				var data = layer.data;

				if (data.encoding == "csv")
				{
					data.tiles = data.value
						.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
						.Select(uint.Parse)
						.Select(gid => new TmxDataTile((uint)gid))
						.ToList();
				}
				else if (data.encoding == "base64")
				{
					var encodedData = data.value.Trim();
					var decodedData = Convert.FromBase64String(encodedData);

					using (var stream = OpenStream(decodedData, data.compression))
					using (var reader = new BinaryReader(stream))
					{
						data.tiles = new List<TmxDataTile>();

						for (var y = 0; y < layer.width; y++)
						{
							for (var x = 0; x < layer.height; x++)
							{
								var gid = reader.ReadUInt32();
								data.tiles.Add(new TmxDataTile(gid));
							}
						}
					}
				}
			}
			System.Diagnostics.Debug.WriteLine(map);
			context.Logger.LogMessage("Finished processing.");
			return map;
		}

		private static Stream OpenStream(byte[] decodedData, string compressionMode)
		{
			var memoryStream = new MemoryStream(decodedData, writable: false);

			if (compressionMode == "gzip")
				//return new GZipStream( memoryStream, CompressionMode.Decompress );
				Console.WriteLine("gzip compression");
			if (compressionMode == "zlib")
				//return new ZlibStream( memoryStream, CompressionMode.Decompress );
				Console.WriteLine("zlib compression");
			return memoryStream;
		}
	}
}