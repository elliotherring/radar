﻿using System;
using System.IO;

namespace Radar.Pipeline
{
	public static class PathHelper {
		public static string makeRelativePath( string fromPath, string toPath )
		{
			var fromUri = new Uri( fromPath );
			var toUri = new Uri( toPath );

			if( fromUri.Scheme != toUri.Scheme )
				return toPath; // path can't be made relative.

			var relativeUri = fromUri.MakeRelativeUri( toUri );
			var relativePath = Uri.UnescapeDataString( relativeUri.ToString() );

			if( toUri.Scheme.Equals( "file", StringComparison.InvariantCultureIgnoreCase ) )
				relativePath = relativePath.Replace( Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar );

			return relativePath;
		}
	}
}
