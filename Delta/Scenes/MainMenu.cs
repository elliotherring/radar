﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Radar
{
    public class MainMenu : SceneState
    {
    
    	SpriteFont font;

        public MainMenu(GraphicsDevice graphicsDevice)
            : base(graphicsDevice)
        {

        }
        public override void Initialize()
        {

        }

        public override void LoadContent(ContentManager content)
        {
			font = content.Load<SpriteFont>("Fonts/PixelOperator");
        }

        public override void UnloadContent()
        {

        }

        public override void Update(GameTime gameTime)
        {

        }

        public override void Draw()
        {            
   //       spriteBatch.Begin();
			//spriteBatch.DrawString(font, "Display Size", new Vector2(0, 0), Color.White);
			//spriteBatch.DrawString(font, GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width.ToString() + "x" + GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height.ToString(), new Vector2(0, 60), Color.White);                       
          //spriteBatch.End();
        }
        
        public override void OnClientSizeChanged() 
        {
        }
    }
}