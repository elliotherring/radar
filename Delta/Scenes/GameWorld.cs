using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Radar.Collision;
using Radar.Components;
using Radar.Tiled;

namespace Radar
{
	public class World : SceneState
	{
		Room room;
		Room room2;
		SpriteFont font;

		List<int> list = new List<int>();
		Texture2D pink;
		SpriteBatch spriteBatch = new SpriteBatch(Engine.Graphics.GraphicsDevice);

		Actor player;
		FollowCamera camera;
		
		//SoundEffectInstance song;

		public World(GraphicsDevice graphicsDevice) : base(graphicsDevice)
		{

		}

		public override void Initialize()
		{
			camera = new FollowCamera();
			camera.Zoom = 1f;
			player = new Actor();
			camera.Follow(player);
		}

		public override void LoadContent(ContentManager content)
		{
			font = content.Load<SpriteFont>("Fonts/PixelOperator");
			pink = content.Load<Texture2D>("Maps/pink");

			room = content.Load<Room>("Maps/Map");
			room2 = content.Load<Room>("Maps/Map2");

			room.Build();
			room2.Build();
			player.SetSprite(pink);
			
			//quad = new Quad(8, 8, 64, 32);
			//camera.X = 900;
			//camera.Y = 500;
		}

		public override void UnloadContent()
		{

		}

		public override void Update(GameTime gameTime)
		{
			// Poll for current keyboard state
			KeyboardState state = Keyboard.GetState();
			//if (state.IsKeyDown(Keys.Space))
			//{
			//	if (song.State == SoundState.Playing)
			//	{
			//		song.Pause();
			//	}
			//	else
			//	{
			//		song.Resume();
			//	}
			//}
			
			var movespeed = 60f;

			var deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

			if (state.IsKeyDown(Keys.A))
			{
				player.Position.X -= movespeed * deltaTime;
			}
			if (state.IsKeyDown(Keys.D))
			{
				player.Position.X += movespeed * deltaTime;
			}
			if (state.IsKeyDown(Keys.W))
			{
				player.Position.Y -= movespeed * deltaTime;
			}
			if (state.IsKeyDown(Keys.S))
			{
				player.Position.Y += movespeed * deltaTime;
			}
			if (state.IsKeyDown(Keys.Up))
			{
				camera.Zoom += 0.01f;
			}
			if (state.IsKeyDown(Keys.Down))
			{
				camera.Zoom -= 0.01f;
			}
			if (state.IsKeyDown(Keys.Left))
			{
				camera.Angle += 0.01f;
			}
			if (state.IsKeyDown(Keys.Right))
			{
				camera.Angle -= 0.01f;
			}
			if (state.IsKeyDown(Keys.D0))
			{
				camera.Angle = 0;
			}
			if (state.IsKeyDown(Keys.D1))
			{
				camera.Zoom = 1;
			}
			if (state.IsKeyDown(Keys.D2))
			{
				camera.Zoom = 2;
			}
			if (state.IsKeyDown(Keys.D3))
			{
				camera.Zoom = 3;
			}
			if (state.IsKeyDown(Keys.D4))
			{
				camera.Zoom = 4;
			}
			if (state.IsKeyDown(Keys.D5))
			{
				camera.Zoom = 5;
			}
			if (state.IsKeyDown(Keys.D6))
			{
				camera.Zoom = 6;
			}
			//TODO make camera follow object
			
			player.Position.X = (float)Math.Round(player.Position.X,2);
			player.Position.Y = (float)Math.Round(player.Position.Y,2);
			
			room.Update();
			room2.Update();
			camera.Update();

			//TODO quadtree render scaling?
			//TODO STATIC RENDER SIZE YOU SCALE UP FROM ! !  ! ! ! ! ! ! 

			//list.ForEach(Console.WriteLine);

			//stopwatch.Stop();
			//Console.WriteLine("{0} ; {1} - {2}", mouse.X, mouse.Y, list.Count);
		}

		public override void Draw()
		{
			room.Draw(camera);
			room2.Draw(camera);

			//var mouse = Engine.MouseToScreen(camera);

			//quad = new Box((int)mouse.X, (int)mouse.Y, 1, 1);
			spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullCounterClockwise, null, camera.Matrix);
			//(room.Layers[1] as TileLayer).blocks.DrawArea(ref quad, list, pink, spriteBatch);
			spriteBatch.Draw(player.sprite, new Rectangle((int)player.Position.X-16, (int)player.Position.Y-16, player.Bounds.Width, player.Bounds.Height), Color.White);
			spriteBatch.End();
			
			spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullCounterClockwise, null, null);
			spriteBatch.DrawString(font, ""+player.Position.X + ","+player.Position.Y,new Vector2(0,0), Color.Black);
			spriteBatch.DrawString(font, ""+camera.X + ","+camera.Y,new Vector2(0,80), Color.Black);
			spriteBatch.DrawString(font, ""+camera.Round(Engine.RenderScale * Engine.WindowScale * camera.Zoom, 0.025, 0.5),new Vector2(0,160), Color.Black);
			//((m+n-1)/n)*n
			spriteBatch.End();
		}

		public override void OnClientSizeChanged()
		{
			camera.CenterOrigin();
			camera.UpdateMatrices();
		}
	}
}