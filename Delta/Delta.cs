using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Radar.Import;
using Radar.Debug;
using Microsoft.Xna.Framework.Audio;
using System.Diagnostics;

namespace Radar
{
	/// <summary>
	/// This is the main type for your game.
	/// </summary>

	public class Delta : Engine
	{
		SpriteBatch spriteBatch;

		//Debugger debug = new Debugger();
		Settings settings = new Settings();
		SpriteFont font;
		Stopwatch stopwatch;

		public Delta()
		{
			Content.RootDirectory = "Content";
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content. Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			base.Initialize();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			stopwatch = new Stopwatch();
			// Load Up Settings
			// FIXME replace with uh...... default settings class
			Text DefaultSettings = Content.Load<Text>("System/Default Settings");
			settings.ReadInDefaultSettings(DefaultSettings.ToString());
			Text UserSettings = Content.Load<Text>("System/User Settings");
			settings.ReadInUserSettings(UserSettings.ToString());

			// Load Up Controls
			// FIXME switch to just.... controls
			// and store that in a class? i gotta write a class i guess
			Text DefaultControls = Content.Load<Text>("System/Default Controls");

			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = new SpriteBatch(GraphicsDevice);

			// Load in font(s)
			font = Content.Load<SpriteFont>("Fonts/PixelOperator");

			// Create scene manager, and add each screen
			SceneManager.Instance.SetContent(Content);
			SceneManager.Instance.AddScene(new World(GraphicsDevice));

			if (settings.fetch["Fullscreen"] != "True")
			{
				ToggleFullscreen();
			}
			base.LoadContent();
		}

		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// game-specific content.
		/// </summary>
		protected override void UnloadContent()
		{
			SceneManager.Instance.UnloadContent();
		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
			stopwatch = Stopwatch.StartNew();

			if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
				ToggleFullscreen();
			
			SceneManager.Instance.Update(gameTime);
			base.Update(gameTime);
			
			stopwatch.Stop();
			//Console.WriteLine("Update: {0}", stopwatch.Elapsed.TotalMilliseconds);
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{
			stopwatch = Stopwatch.StartNew();
			GraphicsDevice.Clear(Color.Blue);
			SceneManager.Instance.Draw();

			base.Draw(gameTime);
			stopwatch.Stop();
			//Console.WriteLine("Draw: {0}", stopwatch.Elapsed.TotalMilliseconds);
		}

		protected override void OnClientSizeChanged(object sender, EventArgs e)
		{
			base.OnClientSizeChanged(sender, e);
			SceneManager.Instance.OnClientSizeChanged();
		}

		protected override void OnExiting(Object sender, EventArgs args)
		{
			SceneManager.Instance.UnloadContent();
			base.OnExiting(sender, args);
		}
	}
}
