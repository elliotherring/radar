﻿using System;
using Radar.Collision;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Radar.Components
{
	public class Instance : Component
	{
		public Vector2 Position;
	}

	public abstract class Component
	{
		public Collider Bounds;
	}
	
	public class Actor : Instance
	{
		public Texture2D sprite;
		
		public Actor()
		{
			Bounds = new Box(0,0,32,32);
		}
		
		public void SetSprite(Texture2D texture)
		{
			sprite = texture;
		}
	}
}