﻿using System;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using System.Linq;

namespace Radar.Tiled
{
	public class TiledRoomReader : ContentTypeReader<Room>
	{
		protected override Room Read(ContentReader reader, Room existingInstance)
		{

			// Room

			//var x = reader.ReadUInt16();
			//var y = reader.ReadUInt16();
			var width = reader.ReadUInt16();
			var height = reader.ReadUInt16();
			var layers = reader.ReadInt32();

			var level = new Room(
				0, 0, 0, // x y z
				width,
				height,
				layers
			);

			// Tileset
			var textureName = reader.ReadString();
			Texture2D texture = null;
			if (textureName != string.Empty)
			{
				var textureAssetName = reader.getRelativeAssetPath(textureName);
				texture = reader.ContentManager.Load<Texture2D>(textureAssetName);
			}

			level.SetTileset(
				texture: texture,
				firstId: reader.ReadInt32(),
				tileWidth: reader.ReadInt32(),
				tileHeight: reader.ReadInt32(),
				spacing: reader.ReadInt32(),
				margin: reader.ReadInt32()
				);

			List<ushort> animatedtilelist = new List<ushort>();

			var animtiles = reader.ReadInt32();

			for (var i = 0; i < animtiles; i++)
			{
				var id = reader.ReadUInt16();
				var framecount = reader.ReadInt32();
				var anim = new AnimatedTile(id);
				for (var f = 0; f < framecount; f++)
				{
					var frameid = reader.ReadUInt16();
					var duration = reader.ReadSingle();
					anim.Frames.Add(new AnimatedTileFrame(ref level.Tileset, frameid, duration));
				}
				anim.TextureRegion = level.Tileset.TextureRegion(id);
				animatedtilelist.Add(id);
				level.Tileset.Tiles[id] = anim;
			}
			level.Tileset.AnimatedTiles = animatedtilelist.ToArray();

			// Layers
			for (int i = 0; i < layers; i++)
			{

				char type = reader.ReadChar();
				switch (type)
				{
					case 't': level.Layers[i] = ReadTileLayer(reader, level); break;
					case 'i': continue; //level.Layers[i] = ReadImageLayer(reader, level); break;
					//case 'o': level.Layers[i] = ReadObjectLayer(reader, level); break;
					default: continue;
				}
			}

			// Done!
			return level;
		}

		public TileLayer ReadTileLayer(ContentReader reader, Room level)
		{
			var w = (ushort)reader.ReadInt32();
			var h = (ushort)reader.ReadInt32();
			var tilecount = (ushort)reader.ReadInt32();
			var tiles = new ushort[w * h];
			for (int j = 0; j < tilecount; j++)
			{
				tiles[j] = reader.ReadUInt16();
			}
			return new TileLayer(w, h, tiles, level.Tileset.AnimatedTiles, true);
		}

		//public TileLayer ReadImageLayer(ContentReader reader, Room level) {
		//	var w = (ushort)reader.ReadInt32();
		//	var h = (ushort)reader.ReadInt32();

		//	return new ImageLayer(w, h, tiles, level.Tileset.AnimatedTiles);
		//}

		//public TileLayer ReadObjectLayer(ContentReader reader, Room level) {
		//	var w = (ushort)reader.ReadInt32();
		//	var h = (ushort)reader.ReadInt32();
		//	var tilecount = (ushort)reader.ReadInt32();
		//	var tiles = new ushort[w*h];
		//	for (int j = 0; j < tilecount; j++) {
		//		tiles[j] = reader.ReadUInt16();
		//	}
		//	return new ObjectLayer(w, h, tiles, level.Tileset.AnimatedTiles);
		//}
	}
}