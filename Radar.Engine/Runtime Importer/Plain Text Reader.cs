﻿using System;
using System.Text;
using Microsoft.Xna.Framework.Content;

namespace Radar.Import
{
	// Text class for processor and ContentTypeWriter and ContentTypeReader
	public class Text 
	{
			// Text byte array
		byte[] text;
		
		public Text(byte[] text) 
		{
			this.text = text;
		}
		
		// Compiled text property
		public byte[] Contents 
		{
			get {
				return text;
			}
		}
		public override string ToString() {
			return Encoding.UTF8.GetString(text, 0, text.Length);
		}
		
	}

	public class TextReader : ContentTypeReader < Text > 
	{
	// Read the Text data from xnb file
		protected override Text Read(ContentReader input, Text existingInstance) 
		{
			// Read the text length
			int size = input.ReadInt32();
			// Read the text content
			byte[] bytes = input.ReadBytes(size);
			// Generate a Text object with the already read text.
			return new Text(bytes);
		}
	}
}