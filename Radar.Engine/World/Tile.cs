﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Radar.Tiled
{
	public class Tile
	{
		public ushort TileID;
		public virtual ushort id { get { return TileID; } }
		public AnimatedTileFrame frame;
		public Rectangle TextureRegion;
		
		public Tile(ushort id)
		{
			this.TileID = id;
		}
		
		public virtual void Update() 
		{
		
		}
	}
	
	public class OddTile : Tile
	{
		public Dictionary<string,byte> Properties = new Dictionary<string, byte>();
		
		public OddTile(ushort id) : base(id) 
		{

		}
		
		public override void Update()
		{
		
		}
	}
	
	public class AnimatedTile : Tile
	{
		public float ElapsedTime;
		public ushort CurrentFrame = 0;
		public List<AnimatedTileFrame> Frames;

		public AnimatedTileFrame frame { get { return Frames[CurrentFrame]; } }

		public AnimatedTile(ushort id) : base(id)
		{
			this.Frames = new List<AnimatedTileFrame>();
		}
		
		public override void Update() 
		{
			ElapsedTime += Time.deltaTime;

			if( ElapsedTime > Frames[CurrentFrame].duration )
			{	
				if (++CurrentFrame >= Frames.Count) 
				{
					CurrentFrame = 0;
				}
				ElapsedTime = 0;
			}
		}
	}
	
	public class AnimatedTileFrame
	{
		/// <summary>
		/// tileId for this frame of the animation
		/// </summary>
		public readonly ushort tileid;

		/// <summary>
		/// duration in seconds for this frame of the animation
		/// </summary>
		public readonly float duration;
		
		public readonly Vector2[] coordinates;


		public AnimatedTileFrame(ref Tileset Tileset, ushort tileid, float duration )
		{
			this.tileid = tileid;
			this.duration = duration / 1000;
			this.coordinates = new Vector2[4];
            CreateTextureCoordinates(ref Tileset);
		}
		
		private void CreateTextureCoordinates(ref Tileset Tileset)
        {
            var sourceRectangle = Tileset.TextureRegion(tileid);
            var texture = Tileset.texture;
            var texelLeft = (float)sourceRectangle.X / texture.Width;
            var texelTop = (float)sourceRectangle.Y / texture.Height;
            var texelRight = (sourceRectangle.X + sourceRectangle.Width) / (float)texture.Width;
            var texelBottom = (sourceRectangle.Y + sourceRectangle.Height) / (float)texture.Height;

            coordinates[0].X = texelLeft;
            coordinates[0].Y = texelTop;

            coordinates[1].X = texelRight;
            coordinates[1].Y = texelTop;

            coordinates[2].X = texelLeft;
            coordinates[2].Y = texelBottom;

            coordinates[3].X = texelRight;
            coordinates[3].Y = texelBottom;
        }
	}
}