﻿//using System;
//using System.Collections.Generic;
//using System.Drawing;
//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Graphics;

//namespace Radar.Tiled
//{
//	public struct World
//	{
//		// Tiles
//		public ushort WidthInTiles; // in tiles
//		public ushort HeightInTiles;

//		// Graphics
//		public Tileset Tileset;
//		public Room[] Rooms;

//		public LooseCell[,] LooseGrid;
//		public TightCell[,] TightGrid;

//		public World
//			(
//				ushort width, ushort height, int roomcount
//			)
//		{
//			this.WidthInTiles = width;
//			this.HeightInTiles = height;
//			this.Tileset = null;
//			this.Rooms = new Room[roomcount];
//			LooseGrid = null;
//			TightGrid = null;
//		}

//		public void SetTileset(Texture2D texture, int firstId, int tileWidth, int tileHeight, int spacing = 0, int margin = 0)
//		{
//			this.Tileset = new Tileset(texture, firstId, tileWidth, tileHeight, spacing, margin);
//		}

//		public void Build()
//		{
//			foreach (Room room in Rooms)
//			{
//				room.Build();
//			}
//		}
//		public void Update(GameTime gt, Camera bounds)
//		{
//			Tileset.Update();
//			foreach (Room room in Rooms)
//			{
//				room.Update();
//			}
//		}

//		public void Draw(Camera camera)
//		{
//			foreach (Room room in Rooms)
//			{
//				room.Draw(camera);
//			}
//		}
//	}
//}
