﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Radar.Collision;

namespace Radar.Tiled
{
	public abstract class RenderLayer
	{
		public VertexBuffer VertexBuffer;
		public VertexPositionTexture[] Vertices;

		public RenderLayer(int vertexcount)
		{
			this.Vertices = new VertexPositionTexture[vertexcount];
			this.VertexBuffer = CreateVertexBuffer(vertexcount);
		}

		public abstract VertexBuffer CreateVertexBuffer(int vertexcount);

		public void CreateVertices(ref ushort[,] Tiles, ref ushort[] animTiles, ref Tileset tileset)
		{
			List<VertexPositionTexture> vertices = new List<VertexPositionTexture>();
			for (var y = 0; y < Tiles.GetLength(1); y++)
			{
				for (var x = 0; x < Tiles.GetLength(0); x++)
				{
					var tile = Tiles[x, y];
					if (Exclude(tile, ref animTiles)) continue;
					vertices.AddRange(tileset.CreateTileVertices(new Vector2(x * tileset.TileWidth, y * tileset.TileHeight), tile));
				}
			}
			this.Vertices = vertices.ToArray();
		}

		public void BuildVertexBuffer()
		{
			VertexBuffer.SetData(Vertices, 0, Vertices.Length);
		}

		public virtual void ApplyVertexBuffer()
		{
			Engine.Graphics.GraphicsDevice.SetVertexBuffer(VertexBuffer);
		}
		
		public abstract Boolean Exclude(ushort tile, ref ushort[] animTiles);
	}

	public class StaticRenderLayer : RenderLayer
	{
		public StaticRenderLayer(int vertexcount) : base(vertexcount) { }

		public override VertexBuffer CreateVertexBuffer(int vertexcount)
		{
			return new VertexBuffer(Engine.Graphics.GraphicsDevice, typeof(VertexPositionTexture), vertexcount, BufferUsage.WriteOnly);
		}
		
		public override Boolean Exclude(ushort tile, ref ushort[] animTiles)
		{
			return (tile == 0 || animTiles.Contains(tile));
		}
	}

	public class AnimatedRenderLayer : RenderLayer
	{
		public AnimatedRenderLayer(int vertexcount, ushort[] animTiles) : base(vertexcount) { }

		public override VertexBuffer CreateVertexBuffer(int vertexcount)
		{
			return new DynamicVertexBuffer(Engine.Graphics.GraphicsDevice, typeof(VertexPositionTexture), vertexcount, BufferUsage.WriteOnly);
		}

		public void Update(ref Tileset Tileset, ref ushort[] AnimatedTiles)
		{
			int count = 0;
			foreach (ushort t in AnimatedTiles)
			{
				Vector2[] coordinates = (Tileset.Tiles[t] as AnimatedTile).frame.coordinates;
				Vertices[count++].TextureCoordinate = coordinates[0];
				Vertices[count++].TextureCoordinate = coordinates[1];
				Vertices[count++].TextureCoordinate = coordinates[2];
				Vertices[count++].TextureCoordinate = coordinates[3];
			}
			BuildVertexBuffer();
		}
		
		public override Boolean Exclude(ushort tile, ref ushort[] animTiles)
		{
			return (tile == 0 || !animTiles.Contains(tile));
		}
	}

	public class TileLayer : Layer
	{
		public ushort[,] Tiles;
		public ushort[] AnimatedTiles;
		
		public QuadTree<int> blocks;

		public TileLayer(ushort width, ushort height, ushort[] tiles, ushort[] tat, Boolean solid) : base(width, height, solid)
		{
			Solid = solid;
			Tiles = new ushort[width, height];
			for (int i = 0; i < tiles.Length; i++)
				Tiles[i % width, i / width] = tiles[i];

			AnimatedTiles = tiles.Where(i => tat.Contains(i)).ToArray();
			var vertexcount = width * height * 4;

			if (AnimatedTiles.Length > 0)
			{
				RenderLayers = new RenderLayer[2];
				RenderLayers[1] = new AnimatedRenderLayer(vertexcount, AnimatedTiles);
			}
			else
			{
				RenderLayers = new RenderLayer[1];
			}

			RenderLayers[0] = new StaticRenderLayer(vertexcount);			
		}

		public override void Build(ref Tileset tileset, ref Box bounds)
		{
			foreach (RenderLayer renderlayer in RenderLayers)
			{
				renderlayer.CreateVertices(ref Tiles, ref AnimatedTiles, ref tileset);
				renderlayer.BuildVertexBuffer();
			}
			
			blocks = new QuadTree<int>(4, 8, ref bounds);
		}
		
		public void BuildTree(ref Tileset tileset) {
			var i = 0;
			for (var y = 0; y < Tiles.GetLength(1); y++)
			{
				for (var x = 0; x < Tiles.GetLength(0); x++)
				{
					var tile = Tiles[x,y];
					if (tile == 0) continue;
					blocks.Insert(i, x*tileset.TileWidth, y*tileset.TileHeight, tileset.TileWidth, tileset.TileHeight);
					i++;
				}
			}
		}
		
		public override void Update(ref Tileset tileset)
		{
			foreach (AnimatedRenderLayer renderlayer in RenderLayers.OfType<AnimatedRenderLayer>())
			{
				renderlayer.Update(ref tileset, ref AnimatedTiles); 
			}
		}
	}

	public abstract class Layer
	{
		public RenderLayer[] RenderLayers;
		public Boolean Solid;
		
		public Layer(ushort width, ushort height, Boolean Solid = false)
		{

		}

		public abstract void Build(ref Tileset tileset, ref Box bounds);
		
		public abstract void Update(ref Tileset tileset);
	}
}
