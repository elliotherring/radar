﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Radar.Collision;
using Radar.Components;

namespace Radar.Tiled
{
	public class Room
	{
		// Tiles
		public ushort WidthInTiles; // in tiles
		public ushort HeightInTiles;
		public Layer[] Layers;

		public IndexBuffer IndexBuffer;
		public ushort[] Indices;
		public int TriangleCount;
		public Tileset Tileset;
		
		public Box Bounds;
		
		public Room
			(
				int x, int y, int z, ushort width, ushort height, int layers
			)
		{
			this.WidthInTiles = width;
			this.HeightInTiles = height;
			this.Layers = new Layer[layers];
			
			this.Indices = new ushort[width * height * 6];
			this.TriangleCount = Indices.Length / 3;
			this.IndexBuffer = new IndexBuffer(Engine.Graphics.GraphicsDevice, IndexElementSize.SixteenBits, Indices.Length, BufferUsage.WriteOnly);
			this.Tileset = null;			
			this.Bounds = new Box(x,y,1,1);		
		}

		public void SetTileset(Texture2D texture, int firstId, int tileWidth, int tileHeight, int spacing = 0, int margin = 0)
		{
			this.Tileset = new Tileset(texture, firstId, tileWidth, tileHeight, spacing, margin);
			this.Bounds.Width = (WidthInTiles*Tileset.TileWidth);
			this.Bounds.Height = (HeightInTiles*Tileset.TileHeight);
		}

		public void Build()
		{
			CreateIndices();
			for (int i = 0; i < Layers.GetLength(0); i++)
			{
				Layers[i].Build(ref Tileset, ref Bounds);
			}
			(Layers[1] as TileLayer).BuildTree(ref Tileset);
		}

		public void CreateIndices()
		{
			for (int i = 0, j = 0; i < TriangleCount * 3; i += 6, j += 4)
			{
				Indices[i] = (ushort)(0 + j);
				Indices[i + 1] = (ushort)(1 + j);
				Indices[i + 2] = (ushort)(2 + j);
				Indices[i + 3] = (ushort)(1 + j);
				Indices[i + 4] = (ushort)(3 + j);
				Indices[i + 5] = (ushort)(2 + j);
			}
			IndexBuffer.SetData(Indices, 0, Indices.Length);
		}

		public void Update()
		{
			Tileset.Update();
			foreach (Layer layer in Layers)
			{
				layer.Update(ref Tileset);
			}
		}

		public void Draw(Camera camera)
		{
			//TODO offset
			//TODO rest of draw (passes)

			Engine.Effect.Texture = Tileset.texture;
			Engine.Effect.View = camera.Matrix;
			
			Engine.Graphics.GraphicsDevice.Indices = IndexBuffer;

			foreach (Layer layer in Layers)
			{
				foreach (RenderLayer renderlayer in layer.RenderLayers)
				{
					renderlayer.ApplyVertexBuffer();

					foreach (var pass in Engine.Effect.CurrentTechnique.Passes)
					{
						// apply the pass, effectively choosing which vertex shader and fragment (pixel) shader to use
						pass.Apply();
						Engine.Graphics.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, TriangleCount);
					}
				}
			}
		}
	}
}
