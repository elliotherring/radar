﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Radar.Tiled
{
	public struct Segment
	{
		public List<ushort[]> layers;
		public int SegmentX;
		public int SegmentY;
		public int GlobalX;
		public int GlobalY;
		
		public Segment(int x, int y, Map map)
		{
			this.SegmentX = x;
			this.SegmentY = y;
			this.layers = new List<ushort[]>();
			this.GlobalX = SegmentX * map.SegmentWidth * map.Tileset.TileWidth;
			this.GlobalY = SegmentY * map.SegmentHeight * map.Tileset.TileHeight;
		}
		
		public void UpdateLocal() 
		{
			UpdateGlobal();
		}
		
		public void UpdateGlobal() 
		{

		}
		
		public void Draw(SpriteBatch spriteBatch, Map map) {
			foreach (ushort[] layer in layers)
			{
				for (ushort i = 0; i < layer.Length; i++)
				{
					if (layer[i] == 0) { continue; } 
					var tile = map.Tileset.Tiles[layer[i]].id;
					spriteBatch.Draw(map.Tileset.texture, new Vector2(GlobalX + map.Tileset.TileWidth*(i%map.SegmentWidth), GlobalY + map.Tileset.TileHeight*(i/map.SegmentWidth)), map.Tileset.Tiles[tile].TextureRegion, Microsoft.Xna.Framework.Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0F);
				}
			}
		}
	}
}