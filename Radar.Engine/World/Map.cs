﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Radar.Tiled
{
	public struct Map
	{
		// Tiles
		public ushort WidthInTiles; // in tiles
		public ushort HeightInTiles;

		// Graphics
		public byte Scale;
		public Tileset Tileset;
        public GraphicsDevice graphicsDevice;
        
		// Segments

		public Segment[,] Segments;
		public byte SegmentWidth; // in tiles
		public byte SegmentHeight;
		public int WidthInSegments { get { return WidthInTiles / SegmentWidth; } }
		public int HeightInSegments { get { return HeightInTiles / SegmentHeight; } }

		public Map
			(
				ushort width, ushort height,
				byte segmentWidth, byte segmentHeight, byte scale = 1
			)
		{
			this.WidthInTiles = width;
			this.HeightInTiles = height;

			this.Scale = scale;
			this.Tileset = null;

			this.SegmentWidth = segmentWidth;
			this.SegmentHeight = segmentHeight;
			this.Segments = new Segment[(width / segmentWidth), (height / segmentHeight)];
            this.graphicsDevice = null;
		}

		public void SetTileset( Texture2D texture, int firstId, int tileWidth, int tileHeight, int spacing = 0, int margin = 0)
		{
			this.Tileset = new Tileset( texture, firstId, tileWidth, tileHeight, spacing, margin );
		}
	
		public int SegmentGridPositionX(int x) {
			return (x / SegmentWidth / (Tileset.TileHeight*Scale));
		}
		
		public int SegmentGridPositionY(int y) {
			return (y / SegmentHeight / (Tileset.TileHeight*Scale));
		}

		public void Update(GameTime gt, Camera bounds)
		{
			Tileset.Update();
			//int minX, minY, maxX, maxY;
			//minX = Math.Max(SegmentGridPositionX((int)Math.Floor(bounds.Left)),0);
			//maxX = Math.Min(SegmentGridPositionX((int)Math.Ceiling(bounds.Right))+1,WidthInSegments);
			//minY = Math.Max(SegmentGridPositionY((int)Math.Floor(bounds.Top)),0);
			//maxY = Math.Min(SegmentGridPositionY((int)Math.Ceiling(bounds.Bottom))+1,HeightInSegments);
			
			//for( var x = minX; x < maxX; x++ )
			//{
			//	for( var y = minY; y < maxY; y++ )
			//	{
			//		Segments[x,y].UpdateLocal();
			//	}
			//}
			//update global segments if they aren't local
		}
        
		public void Draw(SpriteBatch spriteBatch, Camera bounds)
		{
            int minX, minY, maxX, maxY;
            minX = Math.Max(SegmentGridPositionX((int)Math.Floor(bounds.Left)),0);
            maxX = Math.Min(SegmentGridPositionX((int)Math.Ceiling(bounds.Right))+1,WidthInSegments);
            minY = Math.Max(SegmentGridPositionY((int)Math.Floor(bounds.Top)),0);
            maxY = Math.Min(SegmentGridPositionY((int)Math.Ceiling(bounds.Bottom))+1,HeightInSegments);
            
            for( var x = minX; x < maxX; x++ )
            {
                for( var y = minY; y < maxY; y++ )
                {
                        Segments[x,y].Draw(spriteBatch, this);
                }
            }
        }
	}
}
