using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Radar.Tiled
{
	public class Tileset
	{
		public Texture2D texture;
		public readonly int firstId;
		public readonly int TileWidth;
		public readonly int TileHeight;
		public int spacing;
		public int margin;
		
		// Container for all tiles that includes, at minimum, their ID and texture region
		//public Dictionary<ushort, Tile> Tiles;
		public Tile[] Tiles;
		// Contains a list of all animated tiles
		public ushort[] AnimatedTiles;
		// Contains a list of tiles with properties
		public ushort[] OddTiles;

		public Rectangle[] Regions;

		public Tileset( Texture2D texture, int firstId = 0 )
		{
			this.texture = texture;
			this.firstId = firstId;
		}

		public Tileset( Texture2D texture, int firstId, int TileWidth, int TileHeight, int spacing = 0, int margin = 0)
		{
			this.texture = texture;
			this.firstId = firstId;
			this.TileWidth = TileWidth;
			this.TileHeight = TileHeight;
			this.spacing = spacing;
			this.margin = margin;
			
			SetTextureRegions();
		}
		
		public Rectangle TextureRegion(int i) {
			int x = i%((texture.Width-margin)/TileWidth) - 1;
			int y = i/((texture.Width-margin)/TileWidth);
			return TextureRegion(x, y);
		}
		
		public Rectangle TextureRegion(int x, int y) {
			return new Rectangle(margin+x*(TileWidth), margin+y*(TileHeight), TileWidth, TileHeight );
		}

		public void SetTextureRegions() {
			
			List<Rectangle> regions = new List<Rectangle>();
			List<Tile> tiles = new List<Tile>();
			
			var id = (ushort)firstId;
			for (var i = (ushort)firstId; i < (texture.Width - 2 * margin)/(TileWidth+spacing)*(texture.Height - 2 * margin)/(TileHeight+spacing); i++)
			{
				tiles.Add(new Tile(id));
				regions.Add(TextureRegion(id));
				id++;
			}
			Regions = regions.ToArray();
			Tiles = tiles.ToArray();
		}

		public IEnumerable<VertexPositionTexture> CreateTileVertices(Vector2 position, ushort tile)
		{
			Rectangle sourceRectangle = TextureRegion(tile);
			
			var reciprocalWidth = 1f / texture.Width;
			var reciprocalHeight = 1f / texture.Height;
			
			var texelLeft = (sourceRectangle.X + 0.5f) * reciprocalWidth;
			var texelTop = (sourceRectangle.Y + 0.5f) * reciprocalHeight;
			var texelRight = (sourceRectangle.X + sourceRectangle.Width - 0.1f) * reciprocalWidth;
			var texelBottom = (sourceRectangle.Y + sourceRectangle.Height - 0.1f) * reciprocalHeight;

			VertexPositionTexture vertexTopLeft, vertexTopRight, vertexBottomLeft, vertexBottomRight;

			vertexTopLeft.Position = new Vector3(position, 0);
			vertexTopRight.Position = new Vector3(position + new Vector2(sourceRectangle.Width, 0), 0);
			vertexBottomLeft.Position = new Vector3(position + new Vector2(0, sourceRectangle.Height), 0);
			vertexBottomRight.Position = new Vector3(position + new Vector2(sourceRectangle.Width, sourceRectangle.Height), 0);

			vertexTopLeft.TextureCoordinate.Y = texelTop;
			vertexTopLeft.TextureCoordinate.X = texelLeft;

			vertexTopRight.TextureCoordinate.Y = texelTop;
			vertexTopRight.TextureCoordinate.X = texelRight;

			vertexBottomLeft.TextureCoordinate.Y = texelBottom;
			vertexBottomLeft.TextureCoordinate.X = texelLeft;

			vertexBottomRight.TextureCoordinate.Y = texelBottom;
			vertexBottomRight.TextureCoordinate.X = texelRight;

			yield return vertexTopLeft;
			yield return vertexTopRight;
			yield return vertexBottomLeft;
			yield return vertexBottomRight;
		}
		
		public void Update() {
			foreach(ushort tile in AnimatedTiles) {
				Tiles[tile].Update();
			}
		}
	}
}