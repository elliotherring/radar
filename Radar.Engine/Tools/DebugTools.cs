﻿using System;

namespace Radar.Debug
{

	public class Debugger
	{
	
		SmartFramerate smartFPS = new SmartFramerate(5);
	
		public double Framerate
		{
			get
			{
				return smartFPS.framerate;
			}
		}
		

		public double Memory
		{
			get
			{
				return GC.GetTotalMemory(false) / 1048576f;			
			}
		}

		public void Update(double timeSinceLastFrame)
		{
			smartFPS.Update(timeSinceLastFrame);
		}

	}
	
	class SmartFramerate
	{
		double currentFrametimes;
		double weight;
		int numerator;

		public double framerate
		{
			get
			{
				return Math.Round(numerator / currentFrametimes, 2);
			}
		}

		public SmartFramerate(int oldFrameWeight)
		{
			numerator = oldFrameWeight;
			weight = (double)oldFrameWeight / ((double)oldFrameWeight - 1d);
		}

		public void Update(double timeSinceLastFrame)
		{
			currentFrametimes = currentFrametimes / weight;
			currentFrametimes += timeSinceLastFrame;
		}
	}

}
