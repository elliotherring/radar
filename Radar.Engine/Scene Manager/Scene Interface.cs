﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Radar
{
   interface SceneInterface
   {
       void Initialize();
      
       void LoadContent(ContentManager content);
       
       void UnloadContent();

       void Update(GameTime gameTime);

       void Draw();
       
       void OnClientSizeChanged();
    }
}
