﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Radar
{
   public abstract class SceneState : SceneInterface
   {
      protected GraphicsDevice graphicsDevice;
      public SceneState(GraphicsDevice graphicsDevice)
      {
          this.graphicsDevice = graphicsDevice;
      }
      public abstract void Initialize();
      public abstract void LoadContent(ContentManager content);
      public abstract void UnloadContent();
      public abstract void Update(GameTime gameTime);
      public abstract void Draw();
      public abstract void OnClientSizeChanged();
   }
}
