﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Radar
{
    public class SceneManager
    {
        // Instance of the game state manager     
        private static SceneManager scenes;

        // Stack for the scenes     
        private Stack<SceneState> scenestack = new Stack<SceneState>();

        public static SceneManager Instance
        {
            get
            {
                if (scenes == null)
                {
                    scenes = new SceneManager();
                }
                return scenes;
            }
        }

        // Load content in here
        private ContentManager content;

        public void SetContent(ContentManager content)
        {
            this.content = content;
        }

        // Adds a new scene to the stack 
        public void AddScene(SceneState scene)
        {
            // Add the scene to the stack
            scenestack.Push(scene);
            // Initialize the scene
            scenestack.Peek().Initialize();
            // Call the LoadContent on the scene
            if (content != null)
            {
                scenestack.Peek().LoadContent(content);
            }
        }

        // Removes the top scene from the stack
        public void RemoveScene()
        {
            if (scenestack.Count > 0)
            {
                var scene = scenestack.Peek();
                scenestack.Pop();
            }
        }

        // Clears all the scene from the list
        public void ClearScenes()
        {
            while (scenestack.Count > 0)
            {
                scenestack.Pop();
            }
        }

        // Removes all scenes from the stack and adds a new one 
        public void ChangeScene(SceneState scene)
        {
            ClearScenes();
            AddScene(scene);
        }

        // Updates the top scene. 
        public void Update(GameTime gameTime)
        {
            if (scenestack.Count > 0)
            {
                scenestack.Peek().Update(gameTime);
            }
        }

        // Renders the top scene.
        public void Draw()
        {
            foreach (SceneState scene in scenestack)
            {
                scene.Draw();
            }
        }

        public void DrawTop()
        {
            scenestack.Peek().Draw();
        }

        // Unloads the content from the scene
        public void UnloadContent()
        {
            foreach (SceneState state in scenestack)
            {
                state.UnloadContent();
            }
        }

        public void OnClientSizeChanged()
        {
            foreach (SceneState state in scenestack)
            {
                state.OnClientSizeChanged();
            }
        }
    }
}



