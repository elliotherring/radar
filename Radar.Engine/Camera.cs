﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Radar.Components;
using System;
using System.Drawing;

namespace Radar
{

	public class FollowCamera : Camera
	{
		private Instance subject;
		private float TweenSpeed;

		public FollowCamera()
		{
			TweenSpeed = 0.1f;
		}

		public void Follow(Instance subject)
		{
			this.subject = subject;
		}

		float Lerp(float a, float b, float t)
		{
			//return firstFloat * by + secondFloat * (1 - by);
			return (float)Round(((1f - t) * a + t * b),1,0.5);
		}
		
		public void Update()
		{
			position.X = Lerp(position.X, subject.Position.X, TweenSpeed);
			position.Y = Lerp(position.Y, subject.Position.Y, TweenSpeed);
			base.UpdateMatrices();
		}
	}

	public class Camera
	{
		private Matrix matrix = Matrix.Identity;
		private Matrix inverse = Matrix.Identity;
		public static bool changed;

		internal Vector2 position = Vector2.Zero;
		private Vector2 zoom = Vector2.One;
		private Vector2 origin = Vector2.Zero;
		private float angle = 0;
		private RectangleF bounds;

		public Camera()
		{
			CenterOrigin();
		}

		public override string ToString()
		{
			return "Camera:\n\tPosition: { " + position.X + ", " + position.Y +
				" }\n\tOrigin: { " + origin.X + ", " + origin.Y +
				" }\n\tZoom: { " + zoom.X + ", " + zoom.Y +
				" }\n\tAngle: " + angle;
		}
		
		public double Round(double amountToRound, double nearstOf, double fairness)
		{
		    return Math.Floor(amountToRound / nearstOf + fairness) * nearstOf;
		}


		public virtual void UpdateMatrices()
		{
			var scale = Round(Engine.RenderScale * Engine.WindowScale * Zoom, 0.02, 0.5);
			
			matrix = (
					  Matrix.CreateTranslation(new Vector3(-position, 0))
					* Matrix.CreateRotationZ(angle)
					* Matrix.CreateScale((float)(scale)) //  * Zoom * Engine.WindowScale
					* Matrix.CreateTranslation(new Vector3(origin, 0))
				);
			//THESE HAVE TO BE ORDERED ACCORDING TO ISROT YOU DUMB BABY

			inverse = Matrix.Invert(matrix);

			changed = false;

			CalculateBounds();
		}

		//TODO create static version of these that doesn't need to be updated every frame
		public Vector2 Translation()
		{
			return new Vector2((float)Math.Floor(position.X * Engine.WindowScale * Zoom + 0.5), (float)Math.Floor(position.Y * Engine.WindowScale * Zoom));
		}

		public void CalculateBounds()
		{
			var tl = Vector2.Transform(Vector2.Zero, Inverse);
			var br = Vector2.Transform(new Vector2((Engine.Viewport.X + Engine.Viewport.Width), (Engine.Viewport.Y + Engine.Viewport.Height)), Inverse);

			// If not rotated, use f calculation. 
			if (angle == 0)
			{
				bounds = new RectangleF(tl.X, tl.Y, br.X - tl.X, br.Y - tl.Y);
			}
			else
			{
				var tr = Vector2.Transform(Vector2.UnitX * (Engine.Viewport.X + Engine.Viewport.Width), Inverse);
				var bl = Vector2.Transform(Vector2.UnitY * (Engine.Viewport.Y + Engine.Viewport.Height), Inverse);

				float minX = Math.Min(tl.X, Math.Min(tr.X, Math.Min(bl.X, br.X)));
				float maxX = Math.Max(tl.X, Math.Max(tr.X, Math.Max(bl.X, br.X)));
				float minY = Math.Min(tl.Y, Math.Min(tr.Y, Math.Min(bl.Y, br.Y)));
				float maxY = Math.Max(tl.Y, Math.Max(tr.Y, Math.Max(bl.Y, br.Y)));

				bounds = new RectangleF(minX, minY, maxX - minX, maxY - minY);
			}
		}
		public void CopyFrom(Camera other)
		{
			position = other.position;
			origin = other.origin;
			angle = other.angle;
			zoom = other.zoom;
			changed = true;
		}

		public Matrix Matrix
		{
			get
			{
				if (changed)
					UpdateMatrices();
				return matrix;
			}
		}

		public Matrix Inverse
		{
			get
			{
				if (changed)
					UpdateMatrices();
				return inverse;
			}
		}

		public Vector2 Position
		{
			get { return position; }
			set
			{
				changed = true;
				position = value;
			}
		}

		public Vector2 Origin
		{
			get
			{
				if (changed)
					CenterOrigin();
				return origin;
			}
			set
			{
				changed = true;
				origin = value;
			}
		}

		public float X
		{
			get { return position.X; }
			set
			{
				changed = true;
				position.X = value;
			}
		}

		public float Y
		{
			get { return position.Y; }
			set
			{
				changed = true;
				position.Y = value;
			}
		}

		public float Zoom
		{
			get { return zoom.X; }
			set
			{
				changed = true;
				zoom.X = zoom.Y = (float)Math.Round(value, 2,MidpointRounding.AwayFromZero);
			}
		}

		public float Angle
		{
			get { return angle; }
			set
			{
				changed = true;
				angle = value;
			}
		}

		public RectangleF Bounds
		{
			get
			{
				if (changed)
					UpdateMatrices();
				return bounds;
			}
		}

		public float Left
		{
			get
			{
				if (changed)
					UpdateMatrices();
				return bounds.Left;
			}
		}
		public float Right
		{
			get
			{
				if (changed)
					UpdateMatrices();
				return bounds.Right;
			}
		}
		public float Top
		{
			get
			{
				if (changed)
					UpdateMatrices();
				return bounds.Top;
			}
		}
		public float Bottom
		{
			get
			{
				if (changed)
					UpdateMatrices();
				return bounds.Bottom;
			}
		}

		/*
         *  Utils
         */

		public void CenterOrigin()
		{
			origin = new Vector2((float)Engine.Viewport.Width / 2, (float)Engine.Viewport.Height / 2);
		}

		public void RoundPosition()
		{
			position.X = (float)Math.Round(position.X);
			position.Y = (float)Math.Round(position.Y);
			changed = true;
		}

		public Vector2 ScreenToCamera(Vector2 position)
		{
			return Vector2.Transform(position, Inverse);
		}

		public Vector2 CameraToScreen(Vector2 position)
		{
			return Vector2.Transform(position, Matrix);
		}

		public void Approach(Vector2 position, float ease)
		{
			Position += (position - Position) * ease;
		}

		public void Approach(Vector2 position, float ease, float maxDistance)
		{
			Vector2 move = (position - Position) * ease;
			if (move.Length() > maxDistance)
				Position += Vector2.Normalize(move) * maxDistance;
			else
				Position += move;
		}
	}
}

/*
This Camera class is based on the Monocle engine, which is--

Copyright (c) 2012-2014 Matt Thorson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

(Hey Matt! I know it's polite to put this at the top of source files but I'm still tinkering with this.)
(I'll try and remember to put it up the top when I'm done!)
*/
