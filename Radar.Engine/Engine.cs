using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Radar.Import;
using Radar.Debug;

namespace Radar
{
	/// <summary>
	/// This is the main type for your game.
	/// </summary>

	public class Engine : Game
	{
		public string Title;
		public Version Version;

		public static Engine Instance { get; private set; }
		
		public static int WindowWidth { get; private set; }
		public static int WindowHeight { get; private set; }
		
		public static int ViewWidth { get; private set; }
		public static int ViewHeight { get; private set; }
		
		public static Viewport GUIViewport { get; private set; }
		public static Viewport Viewport { get; private set; }
		
		public static float WindowScale;
		public static int RenderScale;

		public static GraphicsDeviceManager Graphics { get; private set; }
		public static BasicEffect Effect;
		
		public Engine()
		{
			Instance = this;
			Content.RootDirectory = "Content";
			Graphics = new GraphicsDeviceManager(this);
			IsFixedTimeStep = false;
			IsMouseVisible = true;
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content. Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
#if PSX || XBOX
            Graphics.PreferredBackBufferWidth = 1920;
            Graphics.PreferredBackBufferHeight = 1080;
#elif SWITCH
            Graphics.PreferredBackBufferWidth = 1280;
            Graphics.PreferredBackBufferHeight = 720;
#else
			Window.AllowUserResizing = true;
			Window.ClientSizeChanged += OnClientSizeChanged;
			Graphics.HardwareModeSwitch = false;
			//Graphics.SynchronizeWithVerticalRetrace = false; TODO 
			Graphics.PreferredBackBufferWidth = GraphicsDevice.DisplayMode.Width;
			Graphics.PreferredBackBufferHeight = GraphicsDevice.DisplayMode.Height;
#endif
			Graphics.ApplyChanges();

			ViewWidth = WindowWidth = GraphicsDevice.DisplayMode.Width;
			ViewHeight = WindowHeight = GraphicsDevice.DisplayMode.Height;

			RedrawViewport();

			RenderScale = 4;
			Effect = new BasicEffect(GraphicsDevice);
			Effect.TextureEnabled = true;
			
			GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp;
			GraphicsDevice.DepthStencilState = DepthStencilState.None;
			GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
			GraphicsDevice.BlendState = BlendState.AlphaBlend;
			
			base.Initialize();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			base.LoadContent();
		}

		protected override void UnloadContent()
		{
			Content.Unload();
		}
		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
			base.Update(gameTime);
			Time.update((float)gameTime.ElapsedGameTime.TotalSeconds);
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{
			base.Draw(gameTime);
		}

		public void ToggleFullscreen()
		{
			if (Graphics.IsFullScreen)
			{
				SetWindowed();
			}
			else
			{
				SetFullscreen();
			}
			RedrawViewport();
		}

		public void SetWindowed()
		{
			Graphics.PreferredBackBufferWidth = WindowWidth;
			Graphics.PreferredBackBufferHeight = WindowHeight;
			Graphics.IsFullScreen = false;
			Graphics.ApplyChanges();
		}

		public void SetFullscreen()
		{
			Graphics.PreferredBackBufferWidth = this.GraphicsDevice.DisplayMode.Width;
			Graphics.PreferredBackBufferHeight = this.GraphicsDevice.DisplayMode.Height;
			Graphics.IsFullScreen = true;
			Graphics.ApplyChanges();
		}

		/*
        RedrawViewport(), and OnClientSizeChanged() are--
        
        Copyright (c) 2012-2014 Matt Thorson
        
        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:
        
        The above copyright notice and this permission notice shall be included in
        all copies or substantial portions of the Software.
        
        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
        THE SOFTWARE.
        */

		private void RedrawViewport()
		{

			WindowWidth = Window.ClientBounds.Width;
			WindowHeight = Window.ClientBounds.Height;
			
			var targetAspectRatio = ViewWidth / (float)ViewHeight;

			// figure out the largest area that fits in this resolution at the desired aspect ratio
			
			ViewWidth = WindowWidth;
			ViewHeight = (int)(ViewWidth / targetAspectRatio + .5f);


			// set up the new viewport centered in the backbuffer
			Viewport = new Viewport
			{
				X = 0,//(int)(ScreenWidth / 2) - (vwidth / 2),
				Y = (int)(WindowHeight / 2) - (ViewHeight / 2),
				Width = (int)ViewWidth,
				Height = (int)ViewHeight
			};
			
			WindowScale = (float)Math.Round(Math.Min((float)ViewWidth / WindowWidth, (float)ViewHeight / WindowHeight) / ((float)GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height/WindowHeight), 1);
			
			//// set up additional viewport that is centered for GUI
			
			//if (vheight > ScreenHeight)
			//{
			//	vheight = ScreenHeight;
			//	vwidth = (int)(vheight * targetAspectRatio + .5f);
			//}
			
			//GUIViewport = new Viewport
			//{
			//	X = (int)(ScreenWidth / 2) - (vwidth / 2),
			//	Y = (int)(ScreenHeight / 2) - (vheight / 2),
			//	Width = (int)vwidth, // but adjust width? 
			//	Height = (int)vheight
			//};

			GraphicsDevice.Viewport = Viewport;
		}

		protected virtual void OnClientSizeChanged(object sender, EventArgs e)
		{
			Graphics.PreferredBackBufferWidth = Window.ClientBounds.Width;
			Graphics.PreferredBackBufferHeight = Window.ClientBounds.Height;

			if (!Graphics.IsFullScreen)
			{
				WindowWidth = Window.ClientBounds.Width;
				WindowHeight = Window.ClientBounds.Height;
			}
			RedrawViewport();
			Effect.Projection = Matrix.CreateOrthographicOffCenter(0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 0, -100f, 100f);
		}
		
		public static Vector2 MouseToScreen(Camera camera)
		{
			var mouse = Mouse.GetState();
			return camera.ScreenToCamera(new Vector2(mouse.X - Engine.Viewport.X, mouse.Y - Engine.Viewport.Y)); 
		}

		protected override void OnExiting(Object sender, EventArgs args)
		{
			//this.Dispose();
			base.OnExiting(sender, args);
		}
	}
}
