﻿using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Linq;

namespace Radar
{
	public class Settings
	{
		///string defaultSettingsFile = "defaultSettings.txt";
		Dictionary<string,string> settings = new Dictionary<string, string>();
		Dictionary<string,string> defaultSettings = new Dictionary<string, string>();
		
		/// <summary>
		/// This method opens a file using System.IO classes and the
		/// TitleLocation property.  It presumes that a file named
		/// ship.dds has been deployed alongside the game.
		/// </summary>
		
		public Dictionary<string, string> fetch {
			get {
				return this.settings;
			}
		}
        //TODO this makes no sense because if the file is missing then you have to recreate it from source
        // and if you have to recreate it from source then there doesn't need to be a "default settings" file in the first place
		public void ReadInDefaultSettings(string DefaultSettings)
		{
		    this.settings = Array.FindAll(DefaultSettings.Split(new[] {'\n'}, StringSplitOptions.RemoveEmptyEntries), element => element.Contains("="))
               .Select(part => part.Split('='))
               .ToDictionary(split => split[0], split => split[1]);
            this.defaultSettings = settings;
		}
		
		public void ReadInUserSettings(string UserSettings) {
		    var dict = Array.FindAll(UserSettings.Split(new[] {'\n'}, StringSplitOptions.RemoveEmptyEntries), element => element.Contains("="))
               .Select(part => part.Split('='))
               .ToDictionary(split => split[0], split => split[1]);
        	foreach(KeyValuePair<string, string> entry in dict) {
				this.settings[entry.Key] = entry.Value;
				//System.Console.WriteLine(entry.Key + ":" + entry.Value);
			}
    	}
	}
}
	