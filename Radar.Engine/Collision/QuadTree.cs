﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Radar.Collision
{
	/// <summary>
	/// A quad tree where leaf nodes contain a quad and a unique instance of T.
	/// For example, if you are developing a game, you might use QuadTree<GameObject>
	/// for collisions, or QuadTree<int> if you just want to populate it with IDs.
	/// </summary>
	public class QuadTree<T>
	{
		internal static Stack<Branch> branchPool = new Stack<Branch>();
		internal static Stack<Leaf> leafPool = new Stack<Leaf>();

		public Branch root;
		internal int splitCount;
		internal int depthLimit;
		internal Dictionary<T, Leaf> leafLookup = new Dictionary<T, Leaf>();

		/// <summary>
		/// Creates a new QuadTree.
		/// </summary>
		/// <param name="splitCount">How many leaves a branch can hold before it splits into sub-branches.</param>
		/// <param name="depthLimit">Maximum distance a node can be from the tree root.</param>
		/// <param name="region">The region that your quadtree occupies, all inserted quads should fit into this.</param>
		public QuadTree(int splitCount, int depthLimit, ref Box region)
		{
			this.splitCount = splitCount;
			this.depthLimit = depthLimit;
			root = CreateBranch(this, null, 0, ref region);
		}
		/// <summary>
		/// Creates a new QuadTree.
		/// </summary>
		/// <param name="splitCount">How many leaves a branch can hold before it splits into sub-branches.</param>
		/// <param name="depthLimit">Maximum distance a node can be from the tree root.</param>
		/// <param name="region">The region that your quadtree occupies, all inserted quads should fit into this.</param>
		public QuadTree(int splitCount, int depthLimit, Box region)
			: this(splitCount, depthLimit, ref region)
		{

		}
		/// <summary>
		/// Creates a new QuadTree.
		/// </summary>
		/// <param name="splitCount">How many leaves a branch can hold before it splits into sub-branches.</param>
		/// <param name="depthLimit">Maximum distance a node can be from the tree root.</param>
		/// <param name="x">X position of the region.</param>
		/// <param name="y">Y position of the region.</param>
		/// <param name="width">Width of the region.</param>
		/// <param name="height">Height of the region.</param>
		public QuadTree(int splitCount, int depthLimit, int x, int y, int width, int height)
			: this(splitCount, depthLimit, new Box(x, y, width, height))
		{

		}

		/// <summary>
		/// Clear the QuadTree. This will remove all leaves and branches. If you have a lot of moving objects,
		/// you probably want to call Clear() every frame, and re-insert every object. Branches and leaves are pooled.
		/// </summary>
		public void Clear()
		{
			root.Clear();
			root.Tree = this;
			leafLookup.Clear();
		}

		/// <summary>
		/// QuadTree internally keeps pools of Branches and Leaves. If you want to clear these to clean up memory,
		/// you can call this function. Most of the time you'll want to leave this alone, though.
		/// </summary>
		public static void ClearPools()
		{
			branchPool = new Stack<Branch>();
			leafPool = new Stack<Leaf>();
		}

		/// <summary>
		/// Insert a new leaf node into the QuadTree.
		/// </summary>
		/// <param name="value">The leaf value.</param>
		/// <param name="quad">The leaf size.</param>
		public void Insert(T value, ref Box rect)
		{
			Leaf leaf;
			if (!leafLookup.TryGetValue(value, out leaf))
			{
				leaf = CreateLeaf(value, ref rect);
				leafLookup.Add(value, leaf);
			}
			root.Insert(leaf);
		}
		/// <summary>
		/// Insert a new leaf node into the QuadTree.
		/// </summary>
		/// <param name="value">The leaf value.</param>
		/// <param name="quad">The leaf quad.</param>
		public void Insert(T value, Box quad)
		{
			Insert(value, ref quad);
		}
		/// <summary>
		/// Insert a new leaf node into the QuadTree.
		/// </summary>
		/// <param name="value">The leaf value.</param>
		/// <param name="x">X position of the leaf.</param>
		/// <param name="y">Y position of the leaf.</param>
		/// <param name="width">Width of the leaf.</param>
		/// <param name="height">Height of the leaf.</param>
		public void Insert(T value, int x, int y, int width, int height)
		{
			var quad = new Box(x, y, width, height);
			Insert(value, ref quad);
		}

		public void DrawArea(ref Box quad, List<T> values, Texture2D pink, SpriteBatch spriteBatch)
		{
			if (values != null)
				values.Clear();
			else
				values = new List<T>();

			root.DrawQuad(quad, pink, spriteBatch);
			//root.DrawAll(pink, spriteBatch);
		}

		/// <summary>
		/// Find all values contained in the specified area.
		/// </summary>
		/// <returns>True if any values were found.</returns>
		/// <param name="quad">The area to search.</param>
		/// <param name="values">A list to populate with the results. If null, this function will create the list for you.</param>
		public bool SearchArea(ref Box quad, ref List<T> values)
		{
			if (values != null)
				values.Clear();
			else
				values = new List<T>();
			//stopwatch = System.Diagnostics.Stopwatch.StartNew();
			root.SearchQuad(ref quad, values);
			//stopwatch.Stop();
			//Console.WriteLine("Search: {0}", stopwatch.Elapsed.TotalMilliseconds);

			return values.Count > 0;
		}
		/// <summary>
		/// Find all values contained in the specified area.
		/// </summary>
		/// <returns>True if any values were found.</returns>
		/// <param name="quad">The area to search.</param>
		/// <param name="values">A list to populate with the results. If null, this function will create the list for you.</param>
		public bool SearchArea(Box quad, ref List<T> values)
		{
			return SearchArea(ref quad, ref values);
		}
		/// <summary>
		/// Find all values contained in the specified area.
		/// </summary>
		/// <returns>True if any values were found.</returns>
		/// <param name="x">X position to search.</param>
		/// <param name="y">Y position to search.</param>
		/// <param name="width">Width of the search area.</param>
		/// <param name="height">Height of the search area.</param>
		/// <param name="values">A list to populate with the results. If null, this function will create the list for you.</param>
		public bool SearchArea(int x, int y, int width, int height, ref List<T> values)
		{
			var quad = new Box(x, y, width, height);
			return SearchArea(ref quad, ref values);
		}

		/// <summary>
		/// Find all values overlapping the specified point.
		/// </summary>
		/// <returns>True if any values were found.</returns>
		/// <param name="x">The x coordinate.</param>
		/// <param name="y">The y coordinate.</param>
		/// <param name="values">A list to populate with the results. If null, this function will create the list for you.</param>
		public bool SearchPoint(float x, float y, ref List<T> values)
		{
			if (values != null)
				values.Clear();
			else
				values = new List<T>();
			root.SearchPoint(x, y, values);
			return values.Count > 0;
		}

		/// <summary>
		/// Find all other values whose areas are overlapping the specified value.
		/// </summary>
		/// <returns>True if any collisions were found.</returns>
		/// <param name="value">The value to check collisions against.</param>
		/// <param name="values">A list to populate with the results. If null, this function will create the list for you.</param>
		public bool FindCollisions(T value, ref List<T> values)
		{
			if (values != null)
				values.Clear();
			else
				values = new List<T>(leafLookup.Count);

			Leaf leaf;
			if (leafLookup.TryGetValue(value, out leaf))
			{
				var branch = leaf.Branch;

				//Add the leaf's siblings (prevent it from colliding with itself)
				if (branch.Leaves.Count > 0)
					for (int i = 0; i < branch.Leaves.Count; ++i)
						if (leaf != branch.Leaves[i] && leaf.Quad.Intersects(branch.Leaves[i].Quad))
							values.Add(branch.Leaves[i].Value);

				//Add the branch's children
				if (branch.Split)
					for (int i = 0; i < 4; ++i)
						if (branch.Branches[i] != null)
							branch.Branches[i].SearchQuad(ref leaf.Quad, values);

				//Add all leaves back to the root
				branch = branch.Parent;
				while (branch != null)
				{
					if (branch.Leaves.Count > 0)
						for (int i = 0; i < branch.Leaves.Count; ++i)
							if (leaf.Quad.Intersects(branch.Leaves[i].Quad))
								values.Add(branch.Leaves[i].Value);
					branch = branch.Parent;
				}
			}
			return false;
		}

		/// <summary>
		/// Count how many branches are in the QuadTree.
		/// </summary>
		public int CountBranches()
		{
			int count = 0;
			CountBranches(root, ref count);
			return count;
		}
		void CountBranches(Branch branch, ref int count)
		{
			++count;
			if (branch.Split)
				for (int i = 0; i < 4; ++i)
					if (branch.Branches[i] != null)
						CountBranches(branch.Branches[i], ref count);
		}

		static Branch CreateBranch(QuadTree<T> tree, Branch parent, int branchDepth, ref Box quad)
		{
			var branch = branchPool.Count > 0 ? branchPool.Pop() : new Branch();
			branch.Tree = tree;
			branch.Parent = parent;
			branch.Split = false;
			branch.Depth = branchDepth;
			
			int midX = (int)(quad.X + quad.Width * 0.5f);
			int midY = (int)(quad.Y + quad.Height * 0.5f);
			int nwidth = quad.Width/2;
			int nheight = quad.Width/2;
			
			branch.Quads[0] = new Box(quad.X, quad.Y, nwidth, nheight);
			branch.Quads[1] = new Box(midX, quad.Y, nwidth, nheight);
			branch.Quads[2] = new Box(midX, midY, nwidth, nheight);
			branch.Quads[3] = new Box(quad.X, midY, nwidth, nheight);

			return branch;
		}

		static Leaf CreateLeaf(T value, ref Box rect)
		{
			var leaf = leafPool.Count > 0 ? leafPool.Pop() : new Leaf();
			leaf.Value = value;
			leaf.Quad = rect;
			return leaf;
		}

		public class Branch
		{
			public QuadTree<T> Tree;
			public Branch Parent;
			public Box[] Quads = new Box[4];
			public Branch[] Branches = new Branch[4];
			public List<Leaf> Leaves = new List<Leaf>();
			public bool Split;
			public int Depth;

			internal void Clear()
			{
				Tree = null;
				Parent = null;
				Split = false;

				for (int i = 0; i < 4; ++i)
				{
					if (Branches[i] != null)
					{
						branchPool.Push(Branches[i]);
						Branches[i].Clear();
						Branches[i] = null;
					}
				}

				for (int i = 0; i < Leaves.Count; ++i)
				{
					leafPool.Push(Leaves[i]);
					Leaves[i].Branch = null;
					Leaves[i].Value = default(T);
				}

				Leaves.Clear();
			}

			internal void Insert(Leaf leaf)
			{
				//If this branch is already split
				if (Split)
				{
					for (int i = 0; i < 4; ++i)
					{
						if (Quads[i].Contains(leaf.Quad))
						{
							if (Branches[i] == null)
								Branches[i] = CreateBranch(Tree, this, Depth + 1, ref Quads[i]);
							Branches[i].Insert(leaf);
							return;
						}
					}

					Leaves.Add(leaf);
					leaf.Branch = this;
				}
				else
				{
					//Add the leaf to this node
					Leaves.Add(leaf);
					leaf.Branch = this;

					//Once I have reached capacity, split the node
					if (Leaves.Count >= Tree.splitCount && Depth < Tree.depthLimit)
					{
						Split = true;
					}
				}
			}

			internal void SearchQuad(ref Box quad, List<T> values)
			{
				if (Leaves.Count > 0)
					for (int i = 0; i < Leaves.Count; ++i)
						if (quad.Intersects(Leaves[i].Quad))
							values.Add(Leaves[i].Value);
				for (int i = 0; i < 4; ++i)
					if (Branches[i] != null)
					{
						if (Quads[i].Intersects(quad))
						{
							Branches[i].SearchQuad(ref quad, values);
						}
					}
			}

			internal void DrawQuad(Box quad, Texture2D pink, SpriteBatch spriteBatch)
			{
				if (Leaves.Count > 0)
				{
					for (int i = 0; i < Leaves.Count; i++)
					{
						if (!quad.Intersects(Leaves[i].Quad))
						{
							spriteBatch.Draw(pink, new Rectangle(Leaves[i].Quad.Left,Leaves[i].Quad.Top, Leaves[i].Quad.Width, Leaves[i].Quad.Height), Color.White * 0.5f);
						}
					}
				}
				for (int i = 0; i < 4; i++)
				{
					if (Branches[i] != null)
					{
						spriteBatch.Draw(pink, new Rectangle(Quads[i].X, Quads[i].Y, Quads[i].Width, 1), Color.Pink);
						spriteBatch.Draw(pink, new Rectangle(Quads[i].X, Quads[i].Y, 1, Quads[i].Height), Color.Pink);
						spriteBatch.Draw(pink, new Rectangle(Quads[i].X + Quads[i].Width - 1, Quads[i].Y, 1, Quads[i].Height), Color.Pink);
						spriteBatch.Draw(pink, new Rectangle(Quads[i].X, Quads[i].Y + Quads[i].Height - 1, Quads[i].Width, 1), Color.Pink);

						Branches[i].DrawQuad(quad, pink, spriteBatch);
					}
				}
			}

			internal void DrawAll(Texture2D pink, SpriteBatch spriteBatch)
			{
				//if (Leaves.Count > 0)
				//	for (int i = 0; i < Leaves.Count; ++i)
				//		Leaves[i].Quad.Draw(pink, spriteBatch);
				//for (int i = 0; i < 4; ++i)
					//if (Branches[i] != null)
					//{
					//	Branches[i].DrawAll(pink, spriteBatch);
					//}
			}

			internal void SearchPoint(float x, float y, List<T> values)
			{
				if (Leaves.Count > 0)
					for (int i = 0; i < Leaves.Count; ++i)
						if (Leaves[i].Quad.Contains(x, y))
							values.Add(Leaves[i].Value);
				for (int i = 0; i < 4; ++i)
					if (Branches[i] != null)
						Branches[i].SearchPoint(x, y, values);
			}
		}

		public class Leaf
		{
			public Branch Branch;
			public T Value;
			public Box Quad;
		}
	}

	/// <summary>
	/// Used by the QuadTree to represent a rectangular area.
	/// </summary>
	//public struct Quad
	//{

	//	public void Draw(Texture2D pink, SpriteBatch spriteBatch)
	//	{
	//		//Console.WriteLine("Drawing {0}, {1}", MinX, MinY);
	//		spriteBatch.Draw(pink, new Box((int)MinX, (int)MinY, (int)MaxX - (int)MinX, (int)MaxY - (int)MinY), Color.White * 0.5f);
	//	}
	//}
}