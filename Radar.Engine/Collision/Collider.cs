﻿using System;
using Microsoft.Xna.Framework;

namespace Radar.Collision
{
	public abstract class Collider
	{
		public int X;
		public int Y;

		public abstract int Left { get; }
		public abstract int Top { get; }
		public abstract int Right { get; }
		public abstract int Bottom { get; }

		public int Width;
		public int Height;

		public Collider(int x, int y)
		{
			X = x;
			Y = y;
		}
	}

	public class Box : Collider
	{
		public override int Left { get { return X; } }
		public override int Top { get { return Y; } }
		public override int Right { get { return X + Width; } }
		public override int Bottom { get { return Y + Height; } }

		public float HalfWidth { get { return Height / 2; } }
		public float HalfHeight { get { return Height / 2; } }

		public Box(int x, int y, int width, int height) : base(x, y)
		{
			Width = width;
			Height = height;
		}

		public bool Intersects(Box other)
		{
			return Left < other.Right && Right > other.Left && Bottom > other.Top && Top < other.Bottom;
		}

		public bool Intersects(Circle other)
		{
			return Collide.BoxCircle(this, other);
		}
		
		public bool Contains(Box other) 
		{
			return Left <= other.Left && Right >= other.Right && Top <= other.Top && Bottom >= other.Bottom;
		}
		
		public bool Contains(int x, int y)
		{
			return Left <= x && Right >= x && Top <= y && Bottom >= y;
		}
		
		public bool Contains(float x, float y)
		{
			return Contains((int)x, (int)y);
		}
		//public bool Contains(Circle other)
		//{
		//	// TODO 
		//}
	}

	public class Circle : Collider
	{
		public ushort Radius;

		public override int Left { get { return X - Radius; } }
		public override int Top { get { return Y - Radius; } }
		public override int Right { get { return X + Radius; } }
		public override int Bottom { get { return Y + Radius; } }

		public Circle(int x, int y, ushort radius) : base(x, y)
		{
			Radius = radius;
		}

		public bool Intersects(Circle other)
		{
			float XDif = X - other.X;
			float YDif = Y - other.Y;
			float RDif = other.Radius + Radius;

			return XDif * XDif + YDif * YDif <= RDif * RDif;
		}

		public bool Contains(Circle other)
		{
			if (other.Radius > Radius)
			{
				return false;
			}

			float XDif = X - other.X;
			float YDif = Y - other.Y;
			float RDif = Radius - other.Radius;

			return XDif * XDif + YDif * YDif <= RDif * RDif;
		}

		public bool Intersects(Box other)
		{
			return Collide.BoxCircle(other, this);
		}
	}

	public static class Collide
	{
		// https://stackoverflow.com/questions/43485700/xna-monogame-detecting-collision-between-circle-and-rectangle-not-working/43546279#43546279
		public static bool BoxCircle(Box box, Circle circle)
		{
			float rW = box.HalfWidth;
			float rH = box.HalfHeight;

			float distX = Math.Abs(circle.X - box.Left + rW);
			float distY = Math.Abs(circle.Y - box.Top + rH);

			if (distX >= circle.Radius + rW || distY >= circle.Radius + rH)
			{
				return false;
			}

			if (distX < rW || distY < rH)
			{
				return true;
			}

			distX -= rW;
			distY -= rH;

			if (distX * distX + distY * distY < circle.Radius * circle.Radius)
			{
				return true;
			}

			return false;
		}
	}
}