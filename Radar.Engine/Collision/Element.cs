﻿using System;
namespace Radar
{
	internal struct Element
	{
		// Stores the ID of the element. This can be used to associate external
		// data to the element.
		ushort id;

		// Stores the index to the next element in the cell using an indexed SLL.
		ushort next;
	}
}
